<?php

/**
 * @file
 * Plugin to provide access control based on URL variable existence or value.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("URL variable access"),
  'description' => t('Checks if an URL variable exists or has a defined value.'),
  'callback' => 'ctrva_url_variable_access_ctools_access_check',
  'settings form' => 'ctrva_url_variable_access_ctools_access_settings',
  'summary' => 'ctrva_url_variable_access_ctools_access_summary',
);

function ctrva_url_variable_access_ctools_access_settings($form, &$form_state, $conf) {
  $form['settings']['variable-name'] = array(
    '#title' => t('Variable name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $conf['variable-name'],
  );
  $form['settings']['variable-type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => array(
      'exists' => t('Exists'),
      'value' => t('Value'),
    ),
    '#required' => TRUE,
    '#default_value' => $conf['variable-type'],
  );
  $form['settings']['variable-value'] = array(
    '#title' => t('Value'),
    '#type' => 'textfield',
    '#default_value' => $conf['variable-value'],
    '#states' => array(
      'visible' => array(
        ':input[name="settings[variable-type]"]' => array('value' => 'value'),
      ),
    ),
  );
  $form['#validate'][] = 'ctrva_url_variable_access_ctools_access_settings_validate';
  return $form;
}

function ctrva_url_variable_access_ctools_access_settings_validate(&$form, &$form_state) {
  if (($form_state['values']['settings']['variable-type'] == 'value') && empty($form_state['values']['settings']['variable-value'])) {
    form_set_error('variable-value', t('Value is required.'));
  }
}

/**
 * Check for access.
 */
function ctrva_url_variable_access_ctools_access_check($conf, $context) {
  if (isset($_GET[$conf['variable-name']])) {
    if ($conf['variable-type'] == 'exists') {
      return TRUE;
    }
    if ($_GET[$conf['variable-name']] == $conf['variable-value']) {
      return TRUE;
    }
  }
  return FALSE;
}

function ctrva_url_variable_access_ctools_access_summary($conf, $context) {
  return t('URL variable access');
}